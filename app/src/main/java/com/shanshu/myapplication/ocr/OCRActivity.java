package com.shanshu.myapplication.ocr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.baidu.ocr.ui.camera.CameraActivity;
import com.shanshu.myapplication.R;

/**
 * @author 闫善书
 * @description: 文字识别
 * @date :2020/5/11 17:52
 */
public class OCRActivity extends Activity implements View.OnClickListener {
    //通用文字识别请求码
    private static final int REQUEST_CODE_GENERAL_BASIC = 106;
    private static final int REQUEST_CODE_GENERAL_WEBIMAGE = 110;
    private String TAG = "GeneralOCRActivity";
    //创建AlertDialog对话框，弹窗展示识别结果。
    private AlertDialog.Builder alertDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr);
        alertDialog = new AlertDialog.Builder(this);
        initView();
    }
    /**
     * 初始化view，添加点击事件
     */

    private void initView() {
        Button general_basic_button = findViewById(R.id.general_basic_button);
        Button general_webimage_button = findViewById(R.id.general_webimage_button);
        Button idcard_button = findViewById(R.id.idcard_button);
        general_basic_button.setOnClickListener(this);
        general_webimage_button.setOnClickListener(this);
        idcard_button.setOnClickListener(this);
    }

    /**
     * 点击事件监听
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.general_basic_button:{//通用文字识别
                //初始化跳转意图。跳转到module的CameraActivity类。
                Intent intent = new Intent(this, CameraActivity.class);
                //传递文件保存绝对路径，用于获取回调结果读取照片。
                intent.putExtra(CameraActivity.KEY_OUTPUT_FILE_PATH,
                        FileUtil.getSaveFile(getApplication()).getAbsolutePath());
                //设置类型为通用文字识别。
                intent.putExtra(CameraActivity.KEY_CONTENT_TYPE,
                        CameraActivity.CONTENT_TYPE_GENERAL);
                //跳转activity，获取CameraActivity回调结果。
                // 参数REQUEST_CODE_GENERAL_BASIC 为你自定义的一个int类型的数值（一般>0）。
                // 当从activityB中返回来的时候会携带回来。
                // 所以你可以用这个参数来判断是从哪个activity中返回的。
                // （前提是你在开启新的activity的时候 要传的requestcode不一样）
                startActivityForResult(intent, REQUEST_CODE_GENERAL_BASIC);

                break;
            }
            case R.id.general_webimage_button:{//网络图片文字识别
                Intent intent = new Intent(this, CameraActivity.class);
                intent.putExtra(CameraActivity.KEY_OUTPUT_FILE_PATH,
                        FileUtil.getSaveFile(getApplication()).getAbsolutePath());
                intent.putExtra(CameraActivity.KEY_CONTENT_TYPE,
                        CameraActivity.CONTENT_TYPE_GENERAL);
                startActivityForResult(intent, REQUEST_CODE_GENERAL_WEBIMAGE);
                break;
            }
            case R.id.idcard_button:{//身份证识别
                Intent intent = new Intent(this, IDCardActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    /**
     * 回调接口
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 识别成功回调，通用文字识别
        if (requestCode == REQUEST_CODE_GENERAL_BASIC && resultCode == Activity.RESULT_OK) {
            //根据默认保存的图片地址，获取图片，调用module封装的通用文字识别方法，添加识别监听，获取回调结果。
            RecognizeService.recGeneralBasic(this, FileUtil.getSaveFile(getApplicationContext()).getAbsolutePath(),
                    new RecognizeService.ServiceListener() {
                        @Override
                        public void onResult(String result) {
                            //弹窗显示回调结果。
                            alertText("通用文字识别",result);
                            Log.d(TAG,"识别结果：" + result);
                        }
                    });
        }else if (requestCode == REQUEST_CODE_GENERAL_WEBIMAGE && resultCode == Activity.RESULT_OK) {
            //根据默认保存的图片地址，获取图片，调用module封装的通用文字识别方法，添加识别监听，获取回调结果。
            RecognizeService.recWebimage(this, FileUtil.getSaveFile(getApplicationContext()).getAbsolutePath(),
                    new RecognizeService.ServiceListener() {
                        @Override
                        public void onResult(String result) {
                            alertText("网络图片文字识别",result);
                            Log.d(TAG,"识别结果：" + result);
                        }
                    });
        }
    }


    private void alertText(final String title, final String message) {
        //在UI线程上运行指定的操作。
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton("确定", null)
                        .show();
            }
        });
    }
}
