/*
 * Copyright (C) 2017 Baidu, Inc. All Rights Reserved.
 */
package com.shanshu.myapplication.ocr;

import java.io.File;

import android.content.Context;

public class FileUtil {
    public static File getSaveFile(Context context) {
        //返回文件系统中文件所在目录的绝对路径
        File file = new File(context.getFilesDir(), "pic.jpg");
        return file;
    }
}
