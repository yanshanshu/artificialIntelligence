package com.shanshu.myapplication.speech;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.speech.EventListener;
import com.baidu.speech.EventManager;
import com.baidu.speech.EventManagerFactory;
import com.baidu.speech.asr.SpeechConstant;
import com.google.gson.Gson;
import com.shanshu.myapplication.R;
import com.shanshu.myapplication.bean.BDResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 闫善书
 * @description: 语音识别 主要步骤为：
 *  第一步：初始化dialog，设置dialog的显示样式，宽高和界面布局
 *  第二步：设置按钮文本显示状态，初始化EventManager对象，创建语音识别管理器
 *  第三步：implements实现自定义输出事件类，在onEvent()方法实现输出事件回调接口。
 *  第四步：设置识别输入参数，点击开始按钮，发送开始事件
 *  第五步：onEvent()方法回调监解析语音识别结果，打印内容。
 *  第六步：添加取消和停止方法。
 * @date :2020/5/13 19:57
 */
public class BDlistenerDialog extends Dialog implements EventListener {
    private Context mContext;
    private String languageType;
    private TextView tran_text;
    private TextView listener_button;
    private State state = State.IDLE;
    ListenerListener listener;
    private TextView Listener_type;
    private ImageView cancel_img;
    private TextView restart_button;
    private EventManager asr;
    protected boolean enableOffline = false; // 测试离线命令词，需要改成true
    private String TAG = "BDlistenerDialog";

    public BDlistenerDialog(Context context, String languageType, ListenerListener l) {
        //设置dialog显示风格，包括背景色，透明度，标题和浮动等
        super(context, R.style.dialog);
        this.mContext = context;
        //设置语音识别的语言。
        this.languageType = languageType;
        this.listener = l;
        //设置弹窗内容
        setContentView(R.layout.dialog_listener);
        //设置弹窗动画
        getWindow().setWindowAnimations(R.style.PopupAnimation);
        DisplayMetrics dm = new DisplayMetrics();
        //获取屏幕大小
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenWidth = dm.widthPixels;
        Window dialogWindow = this.getWindow();
        // 获取对话框当前的参数值
        WindowManager.LayoutParams p = dialogWindow.getAttributes();
        // 高度设置为屏幕的0.4
        p.height = (int) (LayoutParams.WRAP_CONTENT);
        // 宽度设置为屏幕的0.8
        p.width = (int) (screenWidth * 0.95);
        //设置自定义窗口属性
        dialogWindow.setAttributes(p);
        //初始化xml布局
        initView();
        //设置按钮状态，默认为空闲状态。
        setState(State.IDLE);
        //默认空闲状态，初始化EventManager对象
        toggleReco();
    }

    //初始化布局
    private void initView() {
        restart_button = (TextView) findViewById(R.id.restart_button);//重试
        restart_button
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        restart_button.setVisibility(View.GONE);
                        listener_button.setVisibility(View.VISIBLE);
                        start();
                        setState(State.LISTENING);
                    }
                });
        listener_button = (TextView) findViewById(R.id.listener_button);//语音监听
        listener_button
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
//                        WRLongSpeech();
                        listener_button.setText("正在处理中");
                        listener_button.setBackgroundColor(mContext.getResources().getColor(R.color.b_grey));
                    }
                });
        cancel_img = (ImageView) findViewById(R.id.cancel_img);
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                stop();
                dismiss();
                cancel();
            }
        });
        Listener_type = (TextView) findViewById(R.id.Listener_type);
        tran_text = (TextView) findViewById(R.id.tran_text);
    }

    /**
     * 第二步：
     * 设置状态并更新按钮文本。
     */
    private void setState(State newState) {
        state = newState;
        switch (newState) {
            case IDLE://空闲
                // listener_button.setText("识别");
                Listener_type.setText("识别");
                listener_button.setText("初始化...");
                listener_button.setBackgroundColor(mContext.getResources().getColor(R.color.b_grey));
                listener_button.setClickable(false);
                break;
            case LISTENING://聆听
                // listener_button.setText("聆听中");
                Listener_type.setText("聆听中");
                listener_button.setText("说完了");
                listener_button.setBackgroundColor(android.graphics.Color
                        .parseColor("#3C97DF"));
                listener_button.setClickable(true);
                break;
            case PROCESSING://处理中，按钮禁止点击
                // listener_button.setText("处理中");
                Listener_type.setText("处理中");
                listener_button.setText("正在处理中");
                listener_button.setBackgroundColor(mContext.getResources().getColor(R.color.b_grey));
                listener_button.setClickable(false);
                break;
        }
    }

    /**
     * 根据当前状态，执行初始化，停止和取消操作
     */

    private void toggleReco() {
        switch (state) {
            case IDLE:
                //初始化EventManager对象
                recognize();
                break;
            case LISTENING:
                stopRecording();
                break;
            case PROCESSING:
                cancel();
                break;
        }
    }

    /**
     * 第三步
     * 初始化EventManager对象
     */

    private void recognize() {
        // 初始化EventManager对象,通过工厂创建语音识别的事件管理器。
        // 注意识别事件管理器只能维持一个，请勿同时使用多个实例。
        // 即创建一个新的识别事件管理器后，之前的那个置为null，并不再使用。
        asr = EventManagerFactory.create(mContext, "asr");
        //自定义输出事件类
        asr.registerListener(this);
        if (enableOffline) {
            // 测试离线命令词请开启, 测试 ASR_OFFLINE_ENGINE_GRAMMER_FILE_PATH 参数时开启
            loadOfflineEngine();
        }
        start();
        //初始化完成，聆听中
        setState(State.LISTENING);
    }

    /**
     * 第六步
     * 停止记录用户
     */
    public void stopRecording() {
        Log.d(TAG,"关闭语音：");
        stop();
    }

    /**
     * 第四步
     * 基于SDK集成2.2 发送开始事件
     * 点击开始按钮
     * 测试参数填在这里
     */
    private void start() {
        //设置识别输入参数
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        String event = SpeechConstant.ASR_START; // 替换成测试的event
        if (enableOffline) {
            params.put(SpeechConstant.DECODER, 2);
        }
        // 基于SDK集成2.1 设置识别参数
        //是否需要语音音量数据回调，开启后有CALLBACK_EVENT_ASR_VOLUME事件回调
        params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME, false);
        //控制语音检测的方法(更换参数可以手动结束)
        params.put(SpeechConstant.VAD, SpeechConstant.VAD_DNN);
        if (languageType.equals("zh-CN") || languageType.equals("zh-TW")) {
            params.put(SpeechConstant.PID, 15373); // 中文输入法模型，加强标点（逗号、句号、问号、感叹号）
        } else if (languageType.equals("zh-HK")) {
            params.put(SpeechConstant.PID, 16372); // 粤语，加强标点（逗号、句号、问号、感叹号）
        } else if (languageType.equals("en-GB")) {
            params.put(SpeechConstant.PID, 17372); // 英语，加强标点（逗号、句号、问号、感叹号）
        }
        // 复制此段可以自动检测错误
        (new AutoCheck(mContext, new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 100) {
                    AutoCheck autoCheck = (AutoCheck) msg.obj;
                    synchronized (autoCheck) {
                        String message = autoCheck.obtainErrorMessage(); // autoCheck.obtainAllMessage();
                        // 可以用下面一行替代，在logcat中查看代码
                        Log.w("AutoCheckMessage", message);
                    }
                }
            }
        }, enableOffline)).checkAsr(params);
        // 这里可以替换成你需要测试的json
        String json = new JSONObject(params).toString();
        //发送开始事件
        asr.send(event, json, null, 0, 0);
    }

    /**
     * 点击停止按钮
     * 基于SDK集成4.1 发送停止事件
     */
    private void stop() {
        Log.d(TAG,"停止识别：ASR_STOP");
        // 发送停止录音事件，提前结束录音等待识别结果
        asr.send(SpeechConstant.ASR_STOP, null, null, 0, 0); //
        if (enableOffline) {
            // 测试离线命令词请开启, 测试 ASR_OFFLINE_ENGINE_GRAMMER_FILE_PATH 参数时开启
            unloadOfflineEngine();
        }
        // 基于SDK集成5.2 退出事件管理器
        // 必须与registerListener成对出现，否则可能造成内存泄露
        asr.unregisterListener(this);
    }

    /**
     * 点击取消按钮
     */
    public  void cancel() {
        // 取消本次识别，取消后将立即停止不会返回识别结果
        asr.send(SpeechConstant.ASR_CANCEL, "{}", null, 0, 0);
    }

    /**
     * enableOffline设为true时，在onCreate中调用
     * 基于SDK离线命令词1.4 加载离线资源(离线时使用)
     */
    private void loadOfflineEngine() {
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(SpeechConstant.DECODER, 2);
        params.put(SpeechConstant.ASR_OFFLINE_ENGINE_GRAMMER_FILE_PATH, "assets://baidu_speech_grammar.bsg");
        asr.send(SpeechConstant.ASR_KWS_LOAD_ENGINE, new JSONObject(params).toString(), null, 0, 0);
    }

    /**
     * enableOffline为true时，在onDestory中调用，与loadOfflineEngine对应
     * 基于SDK集成5.1 卸载离线资源步骤(离线时使用)
     */
    private void unloadOfflineEngine() {
        asr.send(SpeechConstant.ASR_KWS_UNLOAD_ENGINE, null, null, 0, 0); //
    }
    /**
     * 第五步
     * 基于sdk集成1.2 自定义输出事件类 EventListener 回调方法
     * 基于SDK集成3.1 开始回调事件
     */
    @Override
    public void onEvent(String name, String params, byte[] data, int offset, int length) {
        String logTxt = "回调name: " + name;
        if (params != null && !params.isEmpty()) {
            logTxt += " ;回调params :" + params;
        }
        if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_READY)) {
            Log.d(TAG,"引擎就绪，可以开始说话。");
            setState(State.LISTENING);
        } else if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_BEGIN)) {
            Log.d(TAG,"检测到说话开始。");
        } else if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_END)) {
            Log.d(TAG,"检测到说话结束。" + name);
            Log.d(TAG,"检测到说话结束 :" + params + "data:" + data + "offset:" + offset);
            setState(State.PROCESSING);
        } else if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_FINISH)) {
            Log.d(TAG,"识别结束，可能包含错误信息。");
            if (params != null && params.contains("\"error\"")) {
                Log.d(TAG,", 错误领域：" + params.toString());
            } else if (params != null && params.contains("\"sub_error\"")) {
                Log.d(TAG,", 错误码：" + params.toString());
            } else if (params != null && params.contains("\"desc\"")) {
                Log.d(TAG,", 错误描述：" + params.toString());
            }
        } else if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_LONG_SPEECH)) {
            Log.d(TAG,"长语音额外的回调，表示长语音识别结束。");
        } else if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_PARTIAL)) {
            Log.d(TAG,"语音识别：" + params);
            try {
                Gson gson = new Gson();
                JSONObject ja = new JSONObject(params);
                BDResult bdResult = new BDResult();
                bdResult = gson.fromJson(ja.toString(), BDResult.class);
                if (bdResult.getResult_type().equals("final_result")) {
                    if (bdResult.getResults_recognition().size() > 0) {
                        tran_text.setText(bdResult.getResults_recognition().get(0));
                        stopRecording();
                        listener.SuccessListener(tran_text.getText().toString());
                    } else {
                        tran_text.setText("没有检测到内容。");
                        listener_button.setVisibility(View.GONE);
                        restart_button.setVisibility(View.VISIBLE);
                    }
                } else {
                    tran_text.setText(bdResult.getResults_recognition().get(0));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (params != null && params.contains("\"nlu_result\"")) {
                if (length > 0 && data.length > 0) {
                    setState(State.PROCESSING);
                    Log.d(TAG,", data：" + data.toString());
                    Log.d(TAG,", offset：" + offset);
                    Log.d(TAG,", length：" + length);
                    logTxt += ", 语义解析结果：" + new String(data, offset, length);

                }
            }
        } else if (data != null) {
            logTxt += " 回调;data length=" + data.length;
        }
        printLog(logTxt);
    }

    /**
     * 打印日志
     */

    private void printLog(String text) {
        if (true) {
            text += "  ;time=" + System.currentTimeMillis();
        }
        text += "\n";
        Log.i(getClass().getName(), text);
    }

    private enum State {
        IDLE, LISTENING, PROCESSING
    }

}
