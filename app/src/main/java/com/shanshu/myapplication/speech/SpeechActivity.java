package com.shanshu.myapplication.speech;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.baidu.tts.client.SpeechError;
import com.baidu.tts.client.SpeechSynthesizeBag;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.SpeechSynthesizerListener;
import com.baidu.tts.client.TtsMode;
import com.shanshu.myapplication.R;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author 闫善书
 * @description:
 * @date :2020/5/13 19:57
 */
public class SpeechActivity extends FragmentActivity implements View.OnClickListener, SpeechSynthesizerListener {

    private BDlistenerDialog bdldialog;
    private Context mContext;
    private String TAG = "SpeechActivity";
    //  百度语音合成
    public static String AppId = "19868647";
    public static String AppKey = "oKdXTOpBVYgIXBWLNNKaTKeb";
    public static String AppSecret = "GqPBbnDjAO4Rsar9twl6QwFlueEAjewm";
    private SpeechSynthesizer mSpeechSynthesizer;
    private ArrayList<SpeechSynthesizeBag> bags;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech);
        Button asr = findViewById(R.id.bt_asr);
        asr.setOnClickListener(this);
        mContext = this;
        initTTS();
    }

    private void initTTS() {
        //获取 SpeechSynthesizer 实例
        mSpeechSynthesizer = SpeechSynthesizer.getInstance();
        // this 是Context的之类，如Activity
        mSpeechSynthesizer.setContext(this);
        //listener是SpeechSynthesizerListener 的实现类，需要实现您自己的业务逻辑。SDK合成后会对这个类的方法进行回调。
        mSpeechSynthesizer.setSpeechSynthesizerListener(this);
        /*这里只是为了让Demo运行使用的APPID,请替换成自己的id。*/
        mSpeechSynthesizer.setAppId(AppId);
        /*这里只是为了让Demo正常运行使用APIKey,请替换成自己的APIKey*/
        mSpeechSynthesizer.setApiKey(AppKey, AppSecret);
//        mSpeechSynthesizer.auth(TtsMode.ONLINE);  // 纯在线
        // 设置发声的人声音，在线生效
        mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
        // 初始化离在线混合模式，如果只需要在线合成功能，使用 TtsMode.ONLINE
        mSpeechSynthesizer.initTts(TtsMode.ONLINE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_asr:{
                bdldialog = new BDlistenerDialog(mContext, "zh-CN",
                        new ListenerListener() {

                            @Override
                            public void cancelClick() {

                            }

                            @Override
                            public void SuccessListener(String a) {
                                //批量合成
//                                bags = new ArrayList<SpeechSynthesizeBag>();
//                                bags.add(getSpeechSynthesizeBag("开始合成：" + a, "0"));
//                                int speechResult = mSpeechSynthesizer.batchSpeak(bags);
                                //直接播放单语句
                                int speechResult = mSpeechSynthesizer.speak(a);
                                if (speechResult != 0) {
                                    Toast.makeText(mContext, "播放失败，错误码:" + speechResult, Toast.LENGTH_SHORT).show();
                                }
                               //语音合成
                                bdldialog.dismiss();
                            }
                        });
                bdldialog.show();
                bdldialog.setCanceledOnTouchOutside(false);
                bdldialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface arg0) {
                        // ldialog.recoTransaction.stopRecording();
                        bdldialog.cancel();
                    }
                });
                break;
            }
        }
    }
    //批量合成并播放接口
    private SpeechSynthesizeBag getSpeechSynthesizeBag(String text, String utteranceId) {
        SpeechSynthesizeBag speechSynthesizeBag = new SpeechSynthesizeBag();
        //需要合成的文本text的长度不能超过1024个GBK字节。
        speechSynthesizeBag.setText(text);
        speechSynthesizeBag.setUtteranceId(utteranceId);
        return speechSynthesizeBag;
    }


    @Override
    public void onSynthesizeStart(String s) {
        Log.d(TAG,"合成开始");

    }

    /**
     * audioData: 合成的部分数据，可以就这部分数据自行播放或者顺序保存到文件。如果保存到文件的话，是一个pcm可以播放的音频文件。 音频数据是16K采样率，16bits编码，单声道。
     * progress 大致进度。从0 到 “合成文本的字符数”。
     * engineType ； 0：当前的audioData数据数据由在线引擎（百度服务器）合成。
     */

    @Override
    public void onSynthesizeDataArrived(String utteranceId, byte[] audioData, int progress, int engineType) {
        Log.d(TAG,"合成过程中的数据回调接口");
    }

    @Override
    public void onSynthesizeFinish(String s) {
        Log.d(TAG,"在合成完成");
    }

    @Override
    public void onSpeechStart(String s) {
        Log.d(TAG,"播放开始");
    }

    @Override
    public void onSpeechProgressChanged(String s, int i) {
        Log.d(TAG,"播放过程中的回调");
    }

    @Override
    public void onSpeechFinish(String s) {
        Log.d(TAG,"合成结束");
    }

    @Override
    public void onError(String s, SpeechError speechError) {
        Log.d(TAG,"语音合成异常：" + speechError.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSpeechSynthesizer.release();
        mSpeechSynthesizer.stop();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mSpeechSynthesizer.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSpeechSynthesizer.pause();
    }
}
