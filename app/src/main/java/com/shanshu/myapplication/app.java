package com.shanshu.myapplication;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.OnResultListener;
import com.baidu.ocr.sdk.exception.OCRError;
import com.baidu.ocr.sdk.model.AccessToken;

/**
 * @author 闫善书
 * @description:
 * @date :2020/5/11 17:22
 */
public class app extends Application {
    private static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();

        OCR.getInstance(this).initAccessToken(new OnResultListener<AccessToken>() {
            @Override
            public void onResult(AccessToken accessToken) {
                String token = accessToken.getAccessToken();

            }

            @Override
            public void onError(OCRError error) {
                error.printStackTrace();
            }
        }, mContext);
    }

    //返回
    public static Context getContext() {
        return mContext;
    }
}
