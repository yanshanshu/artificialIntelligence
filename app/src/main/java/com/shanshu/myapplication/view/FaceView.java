package com.shanshu.myapplication.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.Nullable;

import com.shanshu.myapplication.bean.FaceResult;

import java.text.DecimalFormat;

/**
 * @author 闫善书
 * @description: 自定义人脸框view
 * @date :2020/5/8 13:53
 */
public class FaceView extends View {
    private Paint mTextPaint;
    private Paint mPaint;
    private String mCorlor = "#42ed45";//画笔色值
    private FaceResult[] mFaces;
    private Context mContext;
    private static final String TAG = "FaceView";
    private boolean isFront = false;//前置
    private int previewWidth;//预览宽度
    private int previewHeight;//预览高度
    private int mDisplayOrientation;//显示方向
    private int mOrientation;
    private double fps;
    public FaceView(Context context) {
        this(context, null);
    }

    public FaceView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FaceView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        mContext = context;
        //获取屏幕大小
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        //初始化人脸框画笔
        mPaint = new Paint();
        //设置画笔的颜色
        mPaint.setColor(Color.parseColor(mCorlor));
        //设置画笔类型
        mPaint.setStyle(Paint.Style.STROKE);
        //设置画笔描边的宽度
        mPaint.setStrokeWidth(10);
        //抗锯齿
        mPaint.setAntiAlias(true);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setDither(true);
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, metrics);
        mTextPaint.setTextSize(size);
        mTextPaint.setColor(Color.GREEN);
        mTextPaint.setStyle(Paint.Style.FILL);
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mFaces != null && mFaces.length > 0) {
            float scaleX = (float) getWidth() / (float) previewWidth;
            float scaleY = (float) getHeight() / (float) previewHeight;
            switch (mDisplayOrientation) {
                case 90:
                case 270:
                    scaleX = (float) getWidth() / (float) previewHeight;
                    scaleY = (float) getHeight() / (float) previewWidth;
                    break;
            }
            canvas.save();
            //预设具有指定旋转的当前矩阵。
            canvas.rotate(-mOrientation);
            RectF rectF = new RectF();
            RectF rectF2 = new RectF();
            for (FaceResult face : mFaces) {
                PointF mid = new PointF();
                face.getMidPoint(mid);
                if (mid.x != 0.0f && mid.y != 0.0f) {
                    float eyesDis = face.eyesDistance();
                    rectF.set(new RectF(
                            (mid.x - eyesDis * 1.2f) * scaleX,
                            (mid.y - eyesDis * 0.65f) * scaleY,
                            (mid.x + eyesDis * 1.2f) * scaleX,
                            (mid.y + eyesDis * 1.75f) * scaleY));
                    rectF2.set(new RectF(
                            (mid.x - eyesDis * 2.2f) * scaleX,
                            (mid.y - eyesDis * 1.65f) * scaleY,
                            (mid.x + eyesDis * 2.2f) * scaleX,
                            (mid.y + eyesDis * 2.75f) * scaleY));
                    if (isFront) {
                        float left = rectF.left;
                        float right = rectF.right;
                        rectF.left = getWidth() - right;
                        rectF.right = getWidth() - left;
                    }
                    canvas.drawRect(rectF, mPaint);
//                    canvas.drawText("ID " + face.getId(), rectF.left, rectF.bottom + mTextPaint.getTextSize(), mTextPaint);
//                    canvas.drawText("Confidence " + face.getConfidence(), rectF.left, rectF.bottom + mTextPaint.getTextSize() * 2, mTextPaint);
//                    canvas.drawText("眼睛距离 " + face.eyesDistance(), rectF.left, rectF.bottom + mTextPaint.getTextSize() * 3, mTextPaint);
                }
            }
            canvas.restore();
        }
        DecimalFormat df2 = new DecimalFormat(".##");
        canvas.drawText("检测到帧/s: " + df2.format(fps) + " @ " + previewWidth + "x" + previewHeight, mTextPaint.getTextSize(), mTextPaint.getTextSize(), mTextPaint);
    }

    public void setOrientation(int orientation) {
        mOrientation = orientation;
    }

    public void setFaces(FaceResult[] faces) {
        mFaces = faces;
        invalidate();
    }

    public void setFPS(double fps) {
        this.fps = fps;
    }

    public void setPreviewWidth(int previewWidth) {
        this.previewWidth = previewWidth;
    }

    public void setPreviewHeight(int previewHeight) {
        this.previewHeight = previewHeight;
    }
    /**
     * 设置显示方向
     */

    public void setDisplayOrientation(int displayOrientation) {
        mDisplayOrientation = displayOrientation;
        invalidate();
    }
    public void setFront(boolean front) {
        isFront = front;
    }
}
