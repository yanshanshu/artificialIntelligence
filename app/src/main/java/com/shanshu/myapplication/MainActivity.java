package com.shanshu.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.baidu.tts.client.SpeechError;
import com.baidu.tts.client.SpeechSynthesizeBag;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.SpeechSynthesizerListener;
import com.baidu.tts.client.TtsMode;
import com.shanshu.myapplication.faceRecognition.FaceActivity;
import com.shanshu.myapplication.faceRecognition.FaceContrastActivity;
import com.shanshu.myapplication.ocr.OCRActivity;
import com.shanshu.myapplication.speech.BDlistenerDialog;
import com.shanshu.myapplication.speech.ListenerListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SpeechSynthesizerListener {

    private static String TAG = "MainActivity";
    //权限数组（互联网权限，相机权限，存储读写权限，麦克风权限录制音频，查看WLAN状态、网络连接（在线语音识别，合成），）
    private String[] permissions = new String[]{Manifest.permission.INTERNET,Manifest.permission.CAMERA,
            Manifest.permission.MODIFY_AUDIO_SETTINGS,Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.RECORD_AUDIO,
            Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE};
    //返回code
    private static final int OPEN_SET_REQUEST_CODE = 100;
    private static final int REQUEST_CODE_GENERAL = 105;
    private BDlistenerDialog bdldialog;
    private Context mContext;
    //  百度语音合成
    public static String AppId = "19868647";
    public static String AppKey = "oKdXTOpBVYgIXBWLNNKaTKeb";
    public static String AppSecret = "GqPBbnDjAO4Rsar9twl6QwFlueEAjewm";
    private SpeechSynthesizer mSpeechSynthesizer;
    private ArrayList<SpeechSynthesizeBag> bags;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initPermissions();
        mContext = this;

    }

    private void initView() {
        Button btDetection = findViewById(R.id.bt_detection);
        btDetection.setOnClickListener(this);//人脸检测按钮点击事件
        Button btContrast = findViewById(R.id.bt_contrast);
        btContrast.setOnClickListener(this);//人脸比对按钮点击事件
        Button generalBasic = findViewById(R.id.general_basic_button);
        generalBasic.setOnClickListener(this);//人脸比对按钮点击事件
        Button speech = findViewById(R.id.bt_speech);
        speech.setOnClickListener(this);

        //获取 SpeechSynthesizer 实例 语音技术
        mSpeechSynthesizer = SpeechSynthesizer.getInstance();
        // this 是Context的之类，如Activity
        mSpeechSynthesizer.setContext(this);
        //listener是SpeechSynthesizerListener 的实现类，需要实现您自己的业务逻辑。SDK合成后会对这个类的方法进行回调。
        mSpeechSynthesizer.setSpeechSynthesizerListener(this);
        /*这里只是为了让Demo运行使用的APPID,请替换成自己的id。*/
        mSpeechSynthesizer.setAppId(AppId);
        /*这里只是为了让Demo正常运行使用APIKey,请替换成自己的APIKey*/
        mSpeechSynthesizer.setApiKey(AppKey, AppSecret);
//        mSpeechSynthesizer.auth(TtsMode.ONLINE);  // 纯在线
        // 设置发声的人声音，在线生效
        mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
        // 初始化离在线混合模式，如果只需要在线合成功能，使用 TtsMode.ONLINE
        mSpeechSynthesizer.initTts(TtsMode.ONLINE);
    }

    /**
     * 注册点击事件
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_speech:{
                bdldialog = new BDlistenerDialog(mContext, "zh-CN",
                        new ListenerListener() {

                            @Override
                            public void cancelClick() {

                            }

                            @Override
                            public void SuccessListener(String a) {
                                //批量合成
//                                bags = new ArrayList<SpeechSynthesizeBag>();
//                                bags.add(getSpeechSynthesizeBag("开始合成：" + a, "0"));
//                                int speechResult = mSpeechSynthesizer.batchSpeak(bags);
                                //直接播放单语句
                                int speechResult = mSpeechSynthesizer.speak(a);
                                if (speechResult != 0) {
                                    Toast.makeText(mContext, "播放失败，错误码:" + speechResult, Toast.LENGTH_SHORT).show();
                                }
                                //语音合成
                                bdldialog.dismiss();
                            }
                        });
                bdldialog.show();
                bdldialog.setCanceledOnTouchOutside(false);
                bdldialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface arg0) {
                        // ldialog.recoTransaction.stopRecording();
                        bdldialog.cancel();
                    }
                });
                break;
            }
            case R.id.bt_contrast:{//人脸对比
                Intent intent = new Intent(this, FaceContrastActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.bt_detection:{//人脸检测
//                initPermissions();
                Intent intent = new Intent(this, FaceActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.general_basic_button:{//图像识别
                Intent intent = new Intent(this, OCRActivity.class);
                startActivity(intent);

//                // 通用文字识别参数设置
//                GeneralBasicParams param = new GeneralBasicParams();
//                param.setDetectDirection(true);
//                param.setImageFile(new File(filePath));
//                // 调用通用文字识别服务
//                OCR.getInstance(this).recognizeGeneralBasic(param, new OnResultListener() {
//                    @Override
//                    public void onResult(GeneralResult result) {
//                        // 调用成功，返回GeneralResult对象
//                        for (WordSimple wordSimple : result.getWordList()) {
//                            // wordSimple不包含位置信息
//                            wordSimple word = wordSimple;
//                            sb.append(word.getWords());
//                            sb.append("\n");
//                        }
//                        // json格式返回字符串
//                        listener.onResult(result.getJsonRes());
//                    }
//                    @Override public void onError(OCRError error) {
//                        // 调用失败，返回OCRError对象
//                    }
//                });
                break;
            }
        }
    }

    //调用此方法判断是否拥有权限
    private void initPermissions(){
        if (lacksPermission()){//判断是否拥有权限
            //请求权限，第二参数权限String数据，第三个参数是请求码便于在onRequestPermissionsResult 方法中根据code进行判断
            ActivityCompat.requestPermissions(this, permissions,OPEN_SET_REQUEST_CODE);
        } else {
            //拥有权限执行操作
        }
    }

    //如果返回true表示缺少权限
    public boolean lacksPermission() {
        for (String permission : permissions) {
            //判断是否缺少权限，true=缺少权限
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
                return true;
            }
        }
        return false;
    }
    /**
     * 重构权限申请回调
     */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){//响应Code
            case OPEN_SET_REQUEST_CODE:
                if (grantResults.length > 0) {
                    //循环判断权限组是否完全申请。
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            Toast.makeText(this,"未拥有相应权限",Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    //拥有权限执行操作
                } else {
                    Toast.makeText(this,"未拥有相应权限",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    //批量合成并播放接口
    private SpeechSynthesizeBag getSpeechSynthesizeBag(String text, String utteranceId) {
        SpeechSynthesizeBag speechSynthesizeBag = new SpeechSynthesizeBag();
        //需要合成的文本text的长度不能超过1024个GBK字节。
        speechSynthesizeBag.setText(text);
        speechSynthesizeBag.setUtteranceId(utteranceId);
        return speechSynthesizeBag;
    }


    @Override
    public void onSynthesizeStart(String s) {
        Log.d(TAG,"合成开始");

    }

    @Override
    public void onSynthesizeDataArrived(String s, byte[] bytes, int i, int i1) {
        Log.d(TAG,"关于综合数据到达");
    }

    @Override
    public void onSynthesizeFinish(String s) {
        Log.d(TAG,"在合成完成");
    }

    @Override
    public void onSpeechStart(String s) {
        Log.d(TAG,"在演讲开始");
    }

    @Override
    public void onSpeechProgressChanged(String s, int i) {
        Log.d(TAG,"关于语言进步变化");
    }

    @Override
    public void onSpeechFinish(String s) {
        Log.d(TAG,"在演讲结束");
    }

    @Override
    public void onError(String s, SpeechError speechError) {
        Log.d(TAG,"语音合成异常：" + speechError.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSpeechSynthesizer.release();
        mSpeechSynthesizer.stop();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mSpeechSynthesizer.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSpeechSynthesizer.pause();
    }

}
