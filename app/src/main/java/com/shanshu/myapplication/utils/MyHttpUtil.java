package com.shanshu.myapplication.utils;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class MyHttpUtil {


    private static final int TIMEOUT_IN_MILLIONS = 15000;

    public interface CallBack
    {
        void onRequestComplete(String result);
    }

   public static  void doGetAsyn(final String urlStr, final CallBack callBack){

                String result = doGet(urlStr);
                if (callBack != null)
                {
                    callBack.onRequestComplete(result);
                }
   }

    /**
     *  /GET 方式请求
     * @param urlStr
     * @return
     */
    public static String doGet(String urlStr){

        URL url = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;

        try {
            url = new URL(urlStr);
            conn= (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(TIMEOUT_IN_MILLIONS);// 设置读取超时
            conn.setConnectTimeout(TIMEOUT_IN_MILLIONS);// 设置连接超时
            conn.setRequestMethod("GET");// 设置请求方式
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");

            if(conn.getResponseCode() ==  200){
                is = conn.getInputStream();
                baos = new ByteArrayOutputStream();
                int len = -1;
                byte[] buf = new byte[128];
                while ((len = is.read(buf)) != -1)
                {
                    baos.write(buf, 0, len);
                }
                baos.flush();
                return baos.toString();
            }else {
                throw new RuntimeException(" responseCode is not 200 ... ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (is != null)
                    is.close();
            } catch (IOException e)
            {
            }
            try
            {
                if (baos != null)
                    baos.close();
            } catch (IOException e)
            {
            }
            conn.disconnect();
        }

        return null;
    }

    /**
     * 异步的Post请求
     * @param urlStr
     * @param params
     * @param callBack
     * @throws Exception
     */
    public static void doPostAsyn(final String urlStr, final String params,
                                  final CallBack callBack) throws Exception
    {
                try
                {
                    String result = doPost(urlStr, params);
                    if (callBack != null)
                    {
                        System.out.println("============tijiao===========");
                        callBack.onRequestComplete(result);
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

}


    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url
     *   发送请求的 URL
     * @param jsonParam
     *   请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     * @throws Exception
     */
    public static String doPost(String url, String jsonParam)
    {
        DataOutputStream out=null;
        BufferedReader reader = null;
        StringBuffer result = new StringBuffer();

        try
        {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpURLConnection conn = (HttpURLConnection) realUrl
                    .openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
//                    "application/x-www-form-urlencoded");
                    "application/json");
            conn.setRequestProperty("charset", "utf-8");
            //HTTP协议header参数：人脸比对需要
//            conn.setRequestProperty("appId", "你的应用ID");
//            conn.setRequestProperty("timestamp", "时间戳");
//            conn.setRequestProperty("nonce", "随机字符串");
//            conn.setRequestProperty("sign", "接口请求签名，待计算");
            conn.setUseCaches(false);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setReadTimeout(TIMEOUT_IN_MILLIONS);
            conn.setConnectTimeout(TIMEOUT_IN_MILLIONS);
            //往服务器写数据
             out =new DataOutputStream(conn.getOutputStream());
             out.writeBytes(jsonParam);
             out.flush();
             out.close();

             reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String lines;
            while ((lines = reader.readLine()) != null) {
                lines = new String(lines.getBytes(), "utf-8");
                result.append(lines);
            }
            System.out.println(result);
            reader.close();
            // 断开连接
            conn.disconnect();

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
                if (reader != null)
                {
                    reader.close();
                }
            } catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        return result.toString();
    }


}
