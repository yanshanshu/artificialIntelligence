package com.shanshu.myapplication.utils;

import android.text.TextUtils;
import android.util.Log;

/**
 * Apr 18, 2019 Create by Martin
 * 日志类，支持普通及超长日志的打印
 * */
public class HHLog {
    private final static int SEGMENT_MAX_SIZE = 3 * 1024;
    private final static int STACK_TRACE_ELEMENT_INDEX = 4;
    private final static int V = 1;
    private final static int D = 2;
    private final static int I = 3;
    private final static int W = 4;
    private final static int E = 5;
    /**
     * 对应Log.e(String tag, String msg)进行打印，日志等级：错误
     * @param tag 标签，可以在logcat工具中被识别
     * @param object 日志内容，需要被打印输出的内容
     * */
    public static void e(String tag, Object object) {
        boolean illegalTag = false;
        boolean illegalObject = false;
        if (TextUtils.isEmpty(tag)) {
            illegalTag = true;
        }
        if (object == null || object.toString().length() == 0) {
            illegalObject = true;
        }
        if (illegalTag || illegalObject) {
            return;
        }
        String message = object.toString();
        int length = message.length();
        if (length <= SEGMENT_MAX_SIZE) {
            formatLog(E, tag, message);
        }else {
            while (message.length() > SEGMENT_MAX_SIZE) {
                String logContent = message.substring(0, SEGMENT_MAX_SIZE);
                message = message.substring(SEGMENT_MAX_SIZE);
                formatLog(E, tag, logContent);
            }
            formatLog(E, tag, message);
        }
    }
    
    /**
     * 对应Log.e(String tag, String msg, Throwable tr)进行打印，日志等级：错误
     * @param tag 标签，可以在logcat工具中被识别
     * @param object 日志内容，需要被打印输出的内容
     * @param thr 异常信息，应一并输出到日志
     * */
    public static void e(String tag, Object object, Throwable thr) {
        //Implementation
    }
    /**
     * 格式化并执行打印
     * @param type 日志等级，包含 5 种：V、D、I、W、E，且只能是这4个
     * @param tag 标签
     * @param object 日志内容
     * */
    private static void formatLog(int type, String tag, Object object) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String filename = stackTraceElements[STACK_TRACE_ELEMENT_INDEX].getFileName();
        String classStr = stackTraceElements[STACK_TRACE_ELEMENT_INDEX].getClassName();
        String methodStr = stackTraceElements[STACK_TRACE_ELEMENT_INDEX].getMethodName();
        int lineNumber = stackTraceElements[STACK_TRACE_ELEMENT_INDEX].getLineNumber();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ (").append(filename).append(":").append(lineNumber).append(")#").append(methodStr).append(" ]");

        String messageContent = object.toString();
        stringBuilder.append(messageContent);
        
        switch (type) {
            case V:
                Log.v(tag, stringBuilder.toString());
                break;
            case D:
                Log.d(tag, stringBuilder.toString());
                break;
            case I:
                Log.i(tag, stringBuilder.toString());
                break;
            case W:
                Log.w(tag, stringBuilder.toString());
                break;
            case E:
                Log.e(tag, stringBuilder.toString());
                break;
            default:
                break;
        }

    }
}
