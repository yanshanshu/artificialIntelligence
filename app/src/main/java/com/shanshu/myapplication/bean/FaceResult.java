package com.shanshu.myapplication.bean;

import android.graphics.PointF;

/**
 * @author 闫善书
 * @description: 人脸信息
 * @date :2020/5/8 14:53
 */

public class FaceResult extends Object {

    private PointF midEye;//mid:两个浮点坐标
    private float eyeDist;//眼睛之间的距离
    private float confidence;//返回一个介于0和1之间的置信因子。这表明如何确定所发现的实际上是一张脸。一个信心因子大于0.3通常就足够了。
    private float pose;//姿势
    private int id;//idFace：人脸
    private long time;

    public FaceResult() {
        id = 0;
        midEye = new PointF(0.0f, 0.0f);
        eyeDist = 0.0f;
        confidence = 0.4f;
        pose = 0.0f;
        time = System.currentTimeMillis();
    }

    //设置人脸信息
    public void setFace(int id, PointF midEye, float eyeDist, float confidence, float pose, long time) {
        set(id, midEye, eyeDist, confidence, pose, time);
    }

    public void clear() {
        set(0, new PointF(0.0f, 0.0f), 0.0f, 0.4f, 0.0f, System.currentTimeMillis());
    }

    public synchronized void set(int id, PointF midEye, float eyeDist, float confidence, float pose, long time) {
        this.id = id;
        this.midEye.set(midEye);
        this.eyeDist = eyeDist;
        this.confidence = confidence;
        this.pose = pose;
        this.time = time;
    }

    public float eyesDistance() {
        return eyeDist;
    }

    public void setEyeDist(float eyeDist) {
        this.eyeDist = eyeDist;
    }

    public void getMidPoint(PointF pt) {
        pt.set(midEye);
    }

    public PointF getMidEye() {
        return midEye;
    }

    public void setMidEye(PointF midEye) {
        this.midEye = midEye;
    }

    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }

    public float getPose() {
        return pose;
    }

    public void setPose(float pose) {
        this.pose = pose;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
