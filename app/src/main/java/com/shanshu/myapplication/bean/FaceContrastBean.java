package com.shanshu.myapplication.bean;

/**
 * @author 闫善书
 * @description:人脸比对返回bean
 * @date :2020/6/9 9:19
 */
public class FaceContrastBean {

    /**
     * code : 0
     * msg : 成功
     * data : {"code":0,"msg":"success","status":"yes","data":{"prob":0.9928858266063378,"rectangle1":{"x":117,"width":81,"y":52,"height":81},"rectangle2":{"x":117,"width":81,"y":52,"height":81}}}
     */

    private int code;
    private String msg;
    private DataBeanX data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * code : 0
         * msg : success
         * status : yes
         * data : {"prob":0.9928858266063378,"rectangle1":{"x":117,"width":81,"y":52,"height":81},"rectangle2":{"x":117,"width":81,"y":52,"height":81}}
         */

        private int code;
        private String msg;
        private String status;
        private DataBean data;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * prob : 0.9928858266063378
             * rectangle1 : {"x":117,"width":81,"y":52,"height":81}
             * rectangle2 : {"x":117,"width":81,"y":52,"height":81}
             */

            private double prob;
            private Rectangle1Bean rectangle1;
            private Rectangle2Bean rectangle2;

            public double getProb() {
                return prob;
            }

            public void setProb(double prob) {
                this.prob = prob;
            }

            public Rectangle1Bean getRectangle1() {
                return rectangle1;
            }

            public void setRectangle1(Rectangle1Bean rectangle1) {
                this.rectangle1 = rectangle1;
            }

            public Rectangle2Bean getRectangle2() {
                return rectangle2;
            }

            public void setRectangle2(Rectangle2Bean rectangle2) {
                this.rectangle2 = rectangle2;
            }

            public static class Rectangle1Bean {
                /**
                 * x : 117.0
                 * width : 81.0
                 * y : 52.0
                 * height : 81.0
                 */

                private double x;
                private double width;
                private double y;
                private double height;

                public double getX() {
                    return x;
                }

                public void setX(double x) {
                    this.x = x;
                }

                public double getWidth() {
                    return width;
                }

                public void setWidth(double width) {
                    this.width = width;
                }

                public double getY() {
                    return y;
                }

                public void setY(double y) {
                    this.y = y;
                }

                public double getHeight() {
                    return height;
                }

                public void setHeight(double height) {
                    this.height = height;
                }
            }

            public static class Rectangle2Bean {
                /**
                 * x : 117.0
                 * width : 81.0
                 * y : 52.0
                 * height : 81.0
                 */

                private double x;
                private double width;
                private double y;
                private double height;

                public double getX() {
                    return x;
                }

                public void setX(double x) {
                    this.x = x;
                }

                public double getWidth() {
                    return width;
                }

                public void setWidth(double width) {
                    this.width = width;
                }

                public double getY() {
                    return y;
                }

                public void setY(double y) {
                    this.y = y;
                }

                public double getHeight() {
                    return height;
                }

                public void setHeight(double height) {
                    this.height = height;
                }
            }
        }
    }
}
