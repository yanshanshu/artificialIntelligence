package com.shanshu.myapplication.faceRecognition;

import android.hardware.Camera;
import android.util.Log;

/**
 * @author 闫善书
 * @description:
 * @date :2020/5/9 9:38
 */
class CameraErrorCallback implements Camera.ErrorCallback {
    private static final String TAG = "CameraErrorCallback";

    public void onError(int error, Camera camera) {
        Log.e(TAG, "遇到意外的相机错误: " + error);
    }
}
