package com.shanshu.myapplication.faceRecognition;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.media.FaceDetector;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.shanshu.myapplication.R;
import com.shanshu.myapplication.bean.FaceResult;
import com.shanshu.myapplication.utils.ImageUtils;
import com.shanshu.myapplication.utils.Util;
import com.shanshu.myapplication.view.FaceView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * @author 闫善书
 * @description: 人脸检测activity
 * @date :2020/5/8 14:35
 */
public class FaceActivity extends Activity implements SurfaceHolder.Callback,Camera.PreviewCallback{
    public static String TAG = "FaceActivity";
    private static Context mContext;
    private static final int faceDetect = 111;
    public Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case faceDetect: {
//                    try {
//                        //加载网络图片
//                        Glide.with(mContext)
//                                .load("http://n.sinaimg.cn/sinacn12/285/w640h445/20180327/955f-fysqfnh3055855.jpg")
//                                .placeholder(R.mipmap.ic_launcher)//占位图
//                                .error(R.mipmap.ic_launcher_round)//加载失败图片
//                                .into(faceImage);
//                        String result = (String) msg.obj;
//                        Gson gson = new Gson();
//                        JSONObject jsonObject = new JSONObject(result);
//                        FaceDetect faceDetect = new FaceDetect();
//                        faceDetect = gson.fromJson(jsonObject.toString(), FaceDetect.class);
//                        //数字格式工具，将数字转化为百分比
//                        NumberFormat percentFormat = NumberFormat.getPercentInstance();
//                        percentFormat.setMaximumFractionDigits(2); //最大小数位数
//                        faceListBeans.clear();
//                        if (faceDetect.getError_code() == 0) {
//                            tvLocation.setText( "\n人脸区域离左边界的距离：" + faceDetect.getResult().getFace_list().get(0).getLocation().getLeft()
//                                    + "\n人脸区域离上边界的距离：" + faceDetect.getResult().getFace_list().get(0).getLocation().getTop()
//                                    + "\n人脸区域的高度：" + faceDetect.getResult().getFace_list().get(0).getLocation().getHeight()
//                                    + "\n人脸区域的宽度：" + faceDetect.getResult().getFace_list().get(0).getLocation().getWidth()
//                                    + "\n人脸框相对于竖直方向的顺时针旋转角：" + faceDetect.getResult().getFace_list().get(0).getLocation().getRotation());
//                            tvGender.setText("性别：" + faceDetect.getResult().getFace_list().get(0).getGender().getType()
//                                    //percentFormat.format(float)自动转换成百分比显示..
//                                    + "。可信度：" + percentFormat.format(faceDetect.getResult().getFace_list().get(0).getGender().getProbability()));
//                            tvBeauty.setText("好看程度打分：" + faceDetect.getResult().getFace_list().get(0).getBeauty());
//                            faceListBeans.add(faceDetect.getResult().getFace_list().get(0));
//                            faceView.setFaces(faceListBeans);
//                        } else {
//                            Toast.makeText(mContext, faceDetect.getError_msg(), Toast.LENGTH_SHORT);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                    break;
                }
                default: {

                    break;
                }
            }
            return false;
        }
    });

    //相机
    private Camera mCamera;
    private int cameraId = 1;//前置1、后置0
    // 让我们跟踪显示的旋转和方向:
    private int mDisplayRotation;//显示旋转
    private int mDisplayOrientation;//显示方向
    //预览宽度
    private int previewWidth;
    //预览高度
    private int previewHeight;
    // 绘制矩形和其他花哨的东西:
    private FaceView mFaceView;
    // 记录所有错误
    private final CameraErrorCallback mErrorCallback = new CameraErrorCallback();
    //要识别的最大面数，最多同时存在的人脸数。
    private static final int MAX_FACE = 10;
    //人脸信息
    private FaceResult[] faces;
    //之前的人脸
    private static FaceResult[] faces_previous;
    // 摄像机数据的表面视图
    private SurfaceView surfaceView;
    //找出可用的相机总数
    private int numberOfCameras;
    //帧率
    private int mFps;
    //prev设置宽度，高度，图像尺寸越小检测速度越快，但是检测距离越短
    private int prevSettingWidth;
    private int prevSettingHeight;
    //识别a中人物的面孔
    private FaceDetector fdet;
    //线程是否占用
    private boolean isThreadWorking = false;
    //开始时间和结束时间
    long start, end;
    int counter = 0;
    // fps检测人脸(不是相机的fps)
    double fps;
    //人脸检测线程
    private FaceDetectThread detectThread = null;
    private int Id = 0;
    //RecylerView脸图像
    private HashMap<Integer, Integer> facesCount = new HashMap<>();
    private Handler handler;
    //切换摄像头
    private ImageView ivExchange;
    //相机ID
    private String BUNDLE_CAMERA_ID = "camera";
    private ImageView ivBack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face);
        mContext = this;
//      初始化
        initView(savedInstanceState);

    }
    /**
     * 第一步：初始化布局view
     */

    public void initView(Bundle savedInstanceState) {
        surfaceView = findViewById(R.id.surfaceView);
        //摄像头转换
        ivExchange =  findViewById(R.id.ivExchange);
        ivBack = findViewById(R.id.iv_back);
        //创建自定义view视图，用来绘制矩形。
        mFaceView = new FaceView(this);
        //向界面添加一个附加的内容视图。
        addContentView(mFaceView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        faces = new FaceResult[MAX_FACE];
        faces_previous = new FaceResult[MAX_FACE];
        for (int i = 0; i < MAX_FACE; i++) {
            faces[i] = new FaceResult();
            faces_previous[i] = new FaceResult();
        }
        //通过Handler来将UI更新操作切换到主线程中执行。
        handler = new Handler();
//      切换前置/后置摄像头
        ivExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraId = (cameraId + 1) % numberOfCameras;
                recreate();//使用新实例重新创建此活动
            }
        });
        //返回与给定键关联的值，如果给定键不存在所需类型的映射，则返回defaultValue。
        if (savedInstanceState != null)
            cameraId = savedInstanceState.getInt(BUNDLE_CAMERA_ID, 0);
        //添加一个返回按钮的点击事件，用来点击返回上一个界面
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //        // Android 4.0 之后不能在主线程中请求HTTP请求
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String assess_token = AuthService.getAuth();
//                faceDetect(assess_token);
//            }
//        }).start();
    }

    /**
     * 第二步，初始化Surface的监听器
     *  acitivty的生命周期
     * onPostCreate是指onCreate方法彻底执行完毕的回调
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // 在访问相机之前，请检查相机权限。如果尚未授予权限，则请求权限。
        //Surface的监听器。提供访问和控制SurfaceView背后的Surface 相关的方法
        SurfaceHolder holder = surfaceView.getHolder();
        //为SurfaceHolder添加一个SurfaceHolder.Callback回调接口。
        holder.addCallback(this);
        //设置像素格式为NV21 手机从摄像头采集的预览数据一般都是NV21
        //NV21 的存储格式是，以4 X 4 图片为例子
        //占用内存为 4 X 4 X 3 / 2 = 24 个字节
        holder.setFormat(ImageFormat.NV21);
    }

    /**
     * 第三步：Surface的监听器  implements（实现）SurfaceHolder.Callback
     * 实现SurfaceHolder.Callback接口中的三个方法，都是在主线程中调用，而不是在绘制线程中调用的
     * 当surface对象创建后，该方法就会被立即调用。
     * 此方法用来初始化相机和预览Surface
     */

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //找出可用的相机总数
        numberOfCameras = Camera.getNumberOfCameras();
        //关于相机的信息
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            //返回特定相机的信息。如果不存在，返回cameraInfo
            Camera.getCameraInfo(i, cameraInfo);
            //0：照相机的正面与屏幕的正面相对。
            //1: 相机的正面和屏幕的正面是一样的。
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                if (cameraId == 0) cameraId = i;
            }
        }
        //创建一个新的相机对象来访问一个特定的硬件相机
        mCamera = Camera.open(cameraId);
        //返回特定相机的信息。如果不存在，返回cameraInfo
        Camera.getCameraInfo(cameraId, cameraInfo);
        //相机的正面和屏幕的正面是一样的。
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            mFaceView.setFront(true);
        }
        //返回此相机服务的当前设置
        Camera.Parameters parameters = mCamera.getParameters();
        //获取相机支持的>=20fps的帧率，用于设置给媒体MediaRecorder
        //因为获取的数值是*1000的，所以要除以1000
        List<int[]> previewFpsRange = parameters.getSupportedPreviewFpsRange();
        for (int[] ints : previewFpsRange) {
            if (ints[0] >= 20000) {
                mFps = ints[0] / 1000;
                Log.e(TAG, "相机帧率：" + mFps);
                break;
            }
        }

        try {
            //设置摄像机数据的表面视图用于预览摄像头实时帧
            mCamera.setPreviewDisplay(surfaceView.getHolder());
        } catch (Exception e) {
            Log.e(TAG, "无法预览图像。", e);
        }
    }

    /**
     * 第三步：Surface的监听器 implements（实现）SurfaceHolder.Callback
     * 实现SurfaceHolder.Callback接口中的三个方法，都是在主线程中调用，而不是在绘制线程中调用的
     * 当surface发生任何结构性的变化时（格式或者大小），该方法就会被立即调用。
     */

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        // 我们没有surface对象，立即返回:
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        // 尝试停止当前预览:
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // Ignore...
        }
        //配置相机，预览帧大小和自动对焦
        configureCamera(width, height);
        // 设置人脸矩形显示方向
        setDisplayOrientation();
        //注册在发生错误时要调用的回调
        setErrorCallback();

        // 创建media.FaceDetector
//        float aspect = (float) previewHeight / (float) previewWidth;
        //创建人脸检测类，此类为谷歌内部封装方法，用来检测人脸，返回Face类数组（多人脸检测）
//        fdet = new FaceDetector(prevSettingWidth, (int) (prevSettingWidth * aspect), MAX_FACE);
        //一切配置就绪!最后再次启动相机预览:
        startPreview();
    }

    /**
     * 第三步：Surface的监听器 implements（实现）SurfaceHolder.Callback
     * 实现SurfaceHolder.Callback接口中的三个方法，都是在主线程中调用，而不是在绘制线程中调用的
     * 当surface对象在将要销毁前，该方法会被立即调用。
     * 销毁摄像头
     */

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.setPreviewCallbackWithBuffer(null);
        mCamera.setErrorCallback(null);
        mCamera.release();
        mCamera = null;
    }

    /**
     * 配置的相机
     */

    private void configureCamera(int width, int height) {
        //返回此相机服务的当前设置
        Camera.Parameters parameters = mCamera.getParameters();
        //设置预览大小
        setOptimalPreviewSize(parameters, width, height);
        //设置自动聚焦:
        setAutoFocus(parameters);
        // 并设置参数
        mCamera.setParameters(parameters);
    }

    /**
     * 设置预览大小
     */

    private void setOptimalPreviewSize(Camera.Parameters cameraParameters, int width, int height) {
        //获取支持的预览大小。
        List<Camera.Size> previewSizes = cameraParameters.getSupportedPreviewSizes();
        float targetRatio = (float) width / height;
        //获得最佳预览大小
        Camera.Size previewSize = Util.getOptimalPreviewSize(this, previewSizes, targetRatio);
        //设置预览宽度和高度
        previewWidth = previewSize.width;
        previewHeight = previewSize.height;

        Log.e(TAG, "预览宽度：" + previewWidth);
        Log.e(TAG, "预览高度：" + previewHeight);

        /**
         * 计算大小以缩放全帧位图到更小的位图，检测缩放位图中的人脸比完整位图具有更高的性能。
         * 图像尺寸越小->检测速度越快，但检测人脸的距离越短，所以计算大小符合你的目的
         */
        if (previewWidth / 4 > 360) {
            prevSettingWidth = 360;
            prevSettingHeight = 270;
        } else if (previewWidth / 4 > 320) {
            prevSettingWidth = 320;
            prevSettingHeight = 240;
        } else if (previewWidth / 4 > 240) {
            prevSettingWidth = 240;
            prevSettingHeight = 160;
        } else {
            prevSettingWidth = 160;
            prevSettingHeight = 120;
        }
        //设置预览图片的尺寸
        cameraParameters.setPreviewSize(previewSize.width, previewSize.height);
        //设置预览宽高
        mFaceView.setPreviewWidth(previewWidth);
        mFaceView.setPreviewHeight(previewHeight);
    }

    /**
     * 设置自动对焦模式
     */

    private void setAutoFocus(Camera.Parameters cameraParameters) {
        //获取支持的焦点模式。
        List<String> focusModes = cameraParameters.getSupportedFocusModes();
        //用于拍照的连续自动对焦模式。
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            //设置焦点模式。连续对焦模式
            cameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
    }

    /**
     * 设置显示方向
     */

    private void setDisplayOrientation() {
        // 现在设置显示方向:
        //获取屏幕的旋转角度
        mDisplayRotation = Util.getDisplayRotation(this);
        //得到显示方向
        mDisplayOrientation = Util.getDisplayOrientation(mDisplayRotation, cameraId);
        //设置预览显示的顺时针旋转角度。
        mCamera.setDisplayOrientation(mDisplayOrientation);
        if (mFaceView != null) {
            //设置人脸举行显示方向显示方向
            mFaceView.setDisplayOrientation(mDisplayOrientation);
        }
    }

    /**
     * 异常回调
     */

    private void setErrorCallback() {
        mCamera.setErrorCallback(mErrorCallback);
    }

    /**
     * 第四步：
     * 界面获取焦点的时候重新启动相机。
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        startPreview();
    }

    /**
     * 第四步：
     * 界面失去焦点的时候停止相机。
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        if (mCamera != null) {
            mCamera.stopPreview();
        }
    }

    /**
     *  启动摄像头的，捕捉和绘制预览帧到屏幕上，并且配置回调
     */

    private void startPreview() {
        //判断摄像头是否初始化
        if (mCamera != null) {
            //线程是否占用
            isThreadWorking = false;
            //开始捕捉和绘制预览帧到屏幕。
            mCamera.startPreview();
            //除了在屏幕上显示外，还为每个预览帧安装一个要调用的回调。
            mCamera.setPreviewCallback(this);
            counter = 0;
        }
    }

    /**
     * 第五步：预览帧回调 implements（实现）Camera.PreviewCallback
     * 获取预览帧数据，返回标准的NV21数据
     */

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (!isThreadWorking) {
            if (counter == 0)
                //返回当前时间(以毫秒为单位)。
                start = System.currentTimeMillis();

            isThreadWorking = true;
            //等待人脸检测线程完成，内部可以进行人脸扣取，保存，绘制人脸框，上传人脸信息等操作
            waitForFdetThreadComplete();
            //创建人脸检测线程，进行人脸扣取，保存，绘制人脸框，上传人脸信息等操作
            detectThread = new FaceDetectThread(handler, this);
            detectThread.setData(data);
            detectThread.start();
        }
    }

    /**
     * 判断人脸检测线程是否完成。
     * 因为一般相机每秒几十帧，
     * 线程中可能会进行人脸上传，扣取，保存等耗时操作，所以需要等待线程完成
     */

    private void waitForFdetThreadComplete() {
        if (detectThread == null) {
            return;
        }
        //测试该线程是否为活动线程。如果线程已经启动且尚未死亡，则该线程是活的。
        if (detectThread.isAlive()) {
            try {
                //等待该线程死亡。
                detectThread.join();
                detectThread = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 第五步：关键位置
     * 在线程中进行人脸检测处理
     */
    private class FaceDetectThread extends Thread {
        private Handler handler;
        private byte[] data = null;
        private Context ctx;
        private Bitmap faceCroped;
        //实例化对象，重载构造方法，主要用来传递数据，完成对象的初始化
        public FaceDetectThread(Handler handler, Context ctx) {
            this.ctx = ctx;
            this.handler = handler;
        }

        public void setData(byte[] data) {
            this.data = data;
        }

        public void run() {
            float aspect = (float) previewHeight / (float) previewWidth;
            int w = prevSettingWidth;//图像尺寸
            int h = (int) (prevSettingWidth * aspect);
            Log.e("FaceDetectThread", "图像尺寸w:" + w + "h:" + h);
            //返回具有指定宽度和高度的可变位图。
            Bitmap bitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.RGB_565);
            //构造一个YuvImage。包含了YUV数据，并且提供了一个将YUV数据压缩成Jpeg数据的方法。 NV21属于YUV数据
            YuvImage yuv = new YuvImage(data, ImageFormat.NV21, bitmap.getWidth(), bitmap.getHeight(), null);
            //用指定的坐标创建一个新的矩形
            Rect rectImage = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            //创建一个新的字节数组输出流。缓冲容量为初始为32字节，但在必要时增加大小。
            ByteArrayOutputStream baout = new ByteArrayOutputStream();
            //将YuvImage中的矩形区域压缩为jpeg。要压缩的矩形区域,0-100,0表示小尺寸压缩，100表示最大质量压缩。
            if (!yuv.compressToJpeg(rectImage, 100, baout)) {
                Log.e("CreateBitmap", "压缩Jpeg失败");
            }
            //创建一个默认的Options对象 此类用于解码Bitmap时的各种参数控制
            BitmapFactory.Options bfo = new BitmapFactory.Options();
            //设置位图格式为RGB_565
            bfo.inPreferredConfig = Bitmap.Config.RGB_565;
            //将输入流解码为位图
            bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(baout.toByteArray()), null, bfo);
            //创建一个新的位图，从现有的位图扩展而来
            Bitmap bmp = Bitmap.createScaledBitmap(bitmap, w, h, false);
            float xScale = (float) previewWidth / (float) prevSettingWidth;
            float yScale = (float) previewHeight / (float) h;
            //相机信息
            Camera.CameraInfo info = new Camera.CameraInfo();
            //返回特定相机的信息。
            Camera.getCameraInfo(cameraId, info);
            ////显示方向
            int rotate = mDisplayOrientation;
//            摄像机面对的方向。它应该是CAMERA_FACING_BACK或CAMERA_FACING_FRONT（摄像头的面和屏幕的面是一样的。）。
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT && mDisplayRotation % 180 == 0) {
                if (rotate + 180 > 360) {
                    rotate = rotate - 180;
                } else
                    rotate = rotate + 180;
            }
            //    旋转位图
            switch (rotate) {
                case 90:
                    bmp = ImageUtils.rotate(bmp, 90);
                    xScale = (float) previewHeight / bmp.getWidth();
                    yScale = (float) previewWidth / bmp.getHeight();
                    break;
                case 180:
                    bmp = ImageUtils.rotate(bmp, 180);
                    break;
                case 270:
                    bmp = ImageUtils.rotate(bmp, 270);
                    xScale = (float) previewHeight / (float) h;
                    yScale = (float) previewWidth / (float) prevSettingWidth;
                    break;
            }
            //创建一个FaceDetector(人脸检测类)，配置与大小的图像到进行分析，并找出能探测到的最大面数。
            //一旦对象被构造，这些参数就不能被改变。注意图像的宽度必须是均匀的。
            fdet = new FaceDetector(bmp.getWidth(), bmp.getHeight(), MAX_FACE);
            //谷歌人脸识别 存储多张人脸的数组变量
            FaceDetector.Face[] fullResults = new FaceDetector.Face[MAX_FACE];
            //查找位图的所有人脸
            fdet.findFaces(bmp, fullResults);
            //要识别的最大面数，最多同时存在的人脸数。
            for (int i = 0; i < MAX_FACE; i++) {
                if (fullResults[i] == null) {
                    //数组为空，清空人脸数组
                    faces[i].clear();
                } else {
//                  PointF与Point完全相同，但X和Y属性的类型是float，而不是int。PointF用于坐标不是整数值的情况。
                    PointF mid = new PointF();
                    //设置眼睛之间的中点位置。mid:面中点的PointF坐标(浮点值)
                    fullResults[i].getMidPoint(mid);
                    mid.x *= xScale;
                    mid.y *= yScale;
//                  eyesDistance:返回眼睛之间的距离。人脸检测其实不是全脸的识别，只是眼睛的检测
                    float eyesDis = fullResults[i].eyesDistance() * xScale;
                    Log.d(TAG,"返回眼睛之间的距离eyesDis:" + eyesDis);
//                  返回一个介于0和1之间的置信因子。这表明如何确定所发现的实际上是一张脸。一个信心因子大于0.3通常就足够了。
                    float confidence = fullResults[i].confidence();
                    Log.d(TAG,"返回一个介于0和1之间的置信因子confidence:" + confidence);
//                  返回脸的姿势。也就是旋转 X、Y或Z轴(三维欧几里得空间中的位置)。
                    float pose = fullResults[i].pose(FaceDetector.Face.EULER_Y);
                    Log.d(TAG,"返回脸的姿势pose:" + pose);
                    int idFace = Id;
//                  用指定的坐标创建一个新矩形。默认只是眼镜的距离大小，可以扩大一定比例来框出全部人脸
//                  注意:没有范围执行检查，因此调用者必须确保左<=右和顶部<=底部。*/
                    Rect rect = new Rect(
                            (int) (mid.x - eyesDis * 1.20f),
                            (int) (mid.y - eyesDis * 0.55f),
                            (int) (mid.x + eyesDis * 1.20f),
                            (int) (mid.y + eyesDis * 1.85f));

                    /**
                     * 只检测脸部大小> 50x50  试验中使用小于50x50的图片可能检测不到人脸
                     */
                    if (rect.height() * rect.width() > 50 * 50) {
                        for (int j = 0; j < MAX_FACE; j++) {
                            //眼睛的距离
                            float eyesDisPre = faces_previous[j].eyesDistance();
                            PointF midPre = new PointF();
                            //中间点位置
                            faces_previous[j].getMidPoint(midPre);
                            RectF rectCheck = new RectF(
                                    (midPre.x - eyesDisPre * 1.65f),
                                    (midPre.y - eyesDisPre * 2.55f),
                                    (midPre.x + eyesDisPre * 1.65f),
                                    (midPre.y + eyesDisPre * 2.35f));
                            //如果(x,y)在矩形内，则返回true。左边和上边是被认为在里面，而右边和底部不在里面。
                            //这意味着对于要包含的x,y:左<= x <右和上<= y <下。空矩形从不包含任何点。
                            if (rectCheck.contains(mid.x, mid.y)) {//被测点的X,Y坐标
                                idFace = faces_previous[j].getId();
                                break;
                            }
                        }
                        if (idFace == Id) Id++;
                        /**
                         * 设置人脸信息
                         * @param dFace：人脸
                         * @param mid:两个浮点坐标
                         * @param eyesDis：眼睛之间的距离
                         * @param confidence:返回一个介于0和1之间的置信因子。
                         * @param pose:返回脸的姿势。也就是旋转 X、Y或Z轴(三维欧几里得空间中的位置)。
                         */
                        faces[i].setFace(idFace, mid, eyesDis, confidence, pose, System.currentTimeMillis());
                        //保存上个人脸
                        faces_previous[i].set(faces[i].getId(), faces[i].getMidEye(), faces[i].eyesDistance(), faces[i].getConfidence(), faces[i].getPose(), faces[i].getTime());
                        // 采取图片面部显示在clerview
                        if (facesCount.get(idFace) == null) {
                            //将指定值与此映射中的指定键关联。
                            facesCount.put(idFace, 0);//人脸图像
                        } else {
                            int count = facesCount.get(idFace) + 1;
                            if (count <= 1)
                                facesCount.put(idFace, count);
                            // 在RecylerView中显示裁剪面
                            if (count == 2) {
                                //获取人脸的位置，用于后面保存到本地，或者上传后台。
                                faceCroped = ImageUtils.cropFace(faces[i], bitmap, rotate);
                                if (faceCroped != null) {
                                    //执行耗时任务，Runnable是要执行的任务代码。意思就是说Runnable的代码实际是在UI线程执行的。可以写更新UI的代码。
                                    handler.post(new Runnable() {
                                        public void run() {
                                            SimpleDateFormat format = new SimpleDateFormat("MM_dd_HH_mm_ss_SSS", Locale.CHINA);// 输出北京时间
                                            String fileName = format.format(new Date()) + ".jpg";
                                            File file = null;
                                            try {
                                                file = ImageUtils.saveFile(faceCroped, fileName);
                                                facesCount.clear();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            //对人脸文件进行操作，比如上传后台等等...
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }

            /**
             * 绘制人脸框
             */

            handler.post(new Runnable() {
                public void run() {
                    //将face发送到FaceView来绘制矩形
                    mFaceView.setFaces(faces);
                    //计算帧
                    end = System.currentTimeMillis();
                    counter++;//
                    double time = (double) (end - start) / 1000;
                    if (time != 0)
                        fps = counter / time;
                    mFaceView.setFPS(fps);
                    if (counter == (Integer.MAX_VALUE - 1000))
                        counter = 0;
                    isThreadWorking = false;
                }
            });

        }
    }


    /**
     * 重要提示代码中所需工具类
     * FileUtil,Base64Util,HttpUtil,GsonUtils请从
     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
     * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
     * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
     * 下载
     */
//    public String faceDetect(String token) {
//        // 请求url
//        String url = "https://aip.baidubce.com/rest/2.0/face/v3/detect";
//        try {
//            Map<String, Object> map = new HashMap<>();
//            //  图片信息(总数据大小应小于10M)，图片上传方式根据image_type来判断
//            map.put("image", "http://n.sinaimg.cn/sinacn12/285/w640h445/20180327/955f-fysqfnh3055855.jpg");
//            //包括age(年龄),beauty(美丑打分，范围0-100，越大表示越美),expression(表情),
//            // face_shape(脸型),gender(性别),glasses(是否带眼镜),landmark(4个关键点位置，左眼中心、右眼中心、鼻尖、嘴中心),
//            // landmark150(150个特征点位置),race(人种),quality（人脸质量信息）,eye_status（双眼状态（睁开/闭合））,
//            // emotion（情绪）,face_type（真实人脸/卡通人脸）,mask（口罩识别）,spoofing（判断图片是否为合成图）信息
//            //逗号分隔. 默认只返回face_token、人脸框、概率和旋转角度
//            map.put("face_field", "gender,beauty");
//            // 图片类型
//            // BASE64:图片的base64值，base64编码后的图片数据，编码后的图片大小不超过2M；
//            // URL:图片的 URL地址( 可能由于网络等原因导致下载图片时间过长)；
//            // FACE_TOKEN: 人脸图片的唯一标识，调用人脸检测接口时，会为每个人脸图片赋予一个唯一的FACE_TOKEN，同一张图片多次检测得到的FACE_TOKEN是同一个。
//            map.put("image_type", "URL");
//            //最多处理人脸的数目，默认值为1，仅检测图片中面积最大的那个人脸；最大值10，检测图片中面积最大的几张人脸。
//            map.put("max_face_num", "1");
//            String param = GsonUtils.toJson(map);
//
//            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
//            String accessToken = token;
//
//            String result = HttpUtil.post(url, accessToken, "application/json", param);
//            //这里是因为android中一切更改UI的操作都不能在子线程中进行，所以我们这里接收到数据之后发送消息到Handler，
//            //通过Handler来将UI更新操作切换到主线程中执行。
//            Message message = new Message();
//            message.what = faceDetect;
//            message.obj = result;
//            mHandler.sendMessage(message);
////            System.out.println(result);
//            Log.d(TAG, result);
//            return result;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
