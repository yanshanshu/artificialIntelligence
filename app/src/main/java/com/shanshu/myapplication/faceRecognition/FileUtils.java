package com.shanshu.myapplication.faceRecognition;

import android.os.Environment;

import com.shanshu.myapplication.app;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    private static String BASE_PATH;
    private static String STICKER_BASE_PATH;

    private static FileUtils mInstance;

    public static FileUtils getInst() {
        if (mInstance == null) {
            synchronized (FileUtils.class) {
                if (mInstance == null) {
                    mInstance = new FileUtils();
                }
            }
        }
        return mInstance;
    }

    public File getExtFile(String path) {
        return new File(BASE_PATH + path);
    }

    /**
     * 获取文件夹大小
     *
     * @param file File实例
     * @return long 单位为K
     * @throws Exception
     */
    public long getFolderSize(File file) {
        try {
            long size = 0;
            if (!file.exists()) {
                return size;
            } else if (!file.isDirectory()) {
                return file.length() / 1024;
            }
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].isDirectory()) {
                    size = size + getFolderSize(fileList[i]);
                } else {
                    size = size + fileList[i].length();
                }
            }
            return size / 1024;
        } catch (Exception e) {
            return 0;
        }
    }


    public String getBasePath(int packageId) {
        return STICKER_BASE_PATH + packageId + "/";
    }


    /**
     * 自定义压缩存储地址
     *
     * @return
     */
    public static String getPath() {
        String path = app.getContext().getApplicationContext().getFilesDir().getAbsolutePath() + "/cstor/image/";
        File file = new File(path);
        if (file.mkdirs()) {
            return path;
        }
        return path;
    }

    //读取assets文件
//    public String readFromAsset(String fileName) {
//        InputStream is = null;
//        BufferedReader br = null;
//        try {
//            is = LifeStyle.getContext().getAssets().open(fileName);
//            br = new BufferedReader(new InputStreamReader(is));
//            String addonStr = "";
//            String line = br.readLine();
//            while (line != null) {
//                addonStr = addonStr + line;
//                line = br.readLine();
//            }
//            return addonStr;
//        } catch (Exception e) {
//            return null;
//        } finally {
//            IOUtil.closeStream(br);
//            IOUtil.closeStream(is);
//        }
////    }

    public void removeAddonFolder(int packageId) {
        String filename = getBasePath(packageId);
        File file = new File(filename);
        if (file.exists()) {
            delete(file);
        }
    }

    public void delete(File file) {
        if (file.isFile()) {
            file.delete();
            return;
        }

        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();
            if (childFiles == null || childFiles.length == 0) {
                file.delete();
                return;
            }

            for (int i = 0; i < childFiles.length; i++) {
                delete(childFiles[i]);
            }

            file.delete();
        }
    }


    public String getPhotoPathForLockWallPaper() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ddz/img";
        File file = new File(path);
        if (!file.getParentFile().exists()) {
            mkdir(file.getParentFile());
        }
        if (!file.exists()) {
            mkdir(file);
        }
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/ddz/img";
//        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/fotoweather/foto weather camera";
    }




    private FileUtils() {
        String sdcardState = Environment.getExternalStorageState();
        //如果没SD卡则放缓存
        if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {
            BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/ddz/";
        } else {
//            BASE_PATH = App.getInstance().getCacheDirPath();
        }

        STICKER_BASE_PATH = BASE_PATH + "/ddz/";
    }

    public boolean createFile(File file) {
        try {
            if (!file.getParentFile().exists()) {
                mkdir(file.getParentFile());
            }
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean mkdir(File file) {
        while (!file.getParentFile().exists()) {
            mkdir(file.getParentFile());
        }
        return file.mkdir();
    }


    public boolean renameDir(String oldDir, String newDir) {
        File of = new File(oldDir);
        File nf = new File(newDir);
        return of.exists() && !nf.exists() && of.renameTo(nf);
    }


}
