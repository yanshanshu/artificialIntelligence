package com.shanshu.myapplication.faceRecognition;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.shanshu.myapplication.R;
import com.shanshu.myapplication.bean.FaceContrastBean;
import com.shanshu.myapplication.utils.BaseRequest;
import com.shanshu.myapplication.utils.MyHttpUtil;

import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author 闫善书
 * @description:人脸相似度比对
 * @date :2020/6/8 11:10
 *
 */
public class FaceContrastActivity extends AppCompatActivity implements View.OnClickListener {
    private List<LocalMedia> selectList = new ArrayList<>();//已选图片
    private Context mContext;
    private static String TAG = "FaceContrastActivity";
    private ImageView ivBack;
    private ImageView zuo;
    private ImageView you;
    private ImageView bidui;
    private String base64zuo;
    private String base64you;

    /**
     * HandlerHandler 是一个消息分发对象。
     * handler是Android给我们提供用来更新UI的一套机制，也是一套消息处理机制，我们可以发消息，也可以通过它处理消息。
     * 这里用来处理网络请求发送过来的消息，展示比对结果。
     */

    public Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 1: {//网络请求回调。
                    String result = (String) msg.obj;
                    Log.d(TAG,"人脸比对结果" + result);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        FaceContrastBean faceContrastBean = new FaceContrastBean();
                        Gson gson = new Gson();
                        faceContrastBean = gson.fromJson(jsonObject.toString(), FaceContrastBean.class);
                        if (faceContrastBean.getCode()==0) {
                            DecimalFormat df = new DecimalFormat("00.00%");
                            similarity.setText("相似度：" + df.format(faceContrastBean.getData().getData().getProb()));
                        } else {
                            Toast.makeText(mContext,faceContrastBean.getMsg(),Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                }
                default:
                    break;
            }
            return false;
        }
    });
    private TextView similarity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_contrast);
        mContext = this;
        initView();
    }
    /**
     * 第一步：初始化布局
     */

    private void initView() {
        similarity = findViewById(R.id.similarity);
        ivBack = findViewById(R.id.iv_back);
        zuo = findViewById(R.id.zuo);
        you = findViewById(R.id.you);
        bidui = findViewById(R.id.iv_bidui);
        ivBack.setOnClickListener(this);
        zuo.setOnClickListener(this);
        you.setOnClickListener(this);
        bidui.setOnClickListener(this);
    }

    /**
     * 第二步：上传或者选择人脸图片
     */

    public void setPhoto(int code) {
        PictureSelector.create(FaceContrastActivity.this)
                .openGallery(PictureMimeType.ofImage())//全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .theme(R.style.picture_default_style)//主题样式(不设置为默认样式) 也可参考demo values/styles下 例如：R.style.picture.white.style
//                .setPictureStyle()// 动态自定义相册主题  注意：此方法最好不要与.theme();同时存在， 二选一
//                .setPictureCropStyle()// 动态自定义裁剪主题
//                .setPictureWindowAnimationStyle()// 自定义相册启动退出动画
                .loadImageEngine(GlideEngine.createGlideEngine())// 外部传入图片加载引擎，必传项
                .isWithVideoImage(false)// 图片和视频是否可以同选,只在ofAll模式下有效
                .isUseCustomCamera(false)// 是否使用自定义相机，5.0以下请不要使用，可能会出现兼容性问题
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)// 设置相册Activity方向，不设置默认使用系统
                .isOriginalImageControl(false)// 是否显示原图控制按钮，如果用户勾选了 压缩、裁剪功能将会失效
                .isWeChatStyle(true)// 是否开启微信图片选择风格，此开关开启了才可使用微信主题！！！
                .isAndroidQTransform(true)// 是否需要处理Android Q 拷贝至应用沙盒的操作，只针对compress(false); && enableCrop(false);有效
//                .bindCustomPlayVideoCallback(callback)// 自定义播放回调控制，用户可以使用自己的视频播放界面
//                .bindPictureSelectorInterfaceListener(interfaceListener)// 提供给用户的一些额外的自定义操作回调
                .isMultipleSkipCrop(true)// 多图裁剪时是否支持跳过，默认支持
                .isMultipleRecyclerAnimation(true)// 多图裁剪底部列表显示动画效果
//                .setLanguage(language)// 设置语言，默认中文
                .maxSelectNum(1)// 最大图片选择数量 int
                .minSelectNum(0)// 最小选择数量 int
                .minVideoSelectNum(1)// 视频最小选择数量，如果没有单独设置的需求则可以不设置，同用minSelectNum字段
                .maxVideoSelectNum(1) // 视频最大选择数量，如果没有单独设置的需求则可以不设置，同用maxSelectNum字段
                .imageSpanCount(4)// 每行显示个数 int
                .isReturnEmpty(false)// 未选择数据时点击按钮是否可以返回
                .isNotPreviewDownload(true)// 预览图片长按是否可以下载
                .queryMaxFileSize(100)// 只查多少M以内的图片、视频、音频  单位M
//                .querySpecifiedFormatSuffix(PictureMimeType.ofPNG())// 查询指定后缀格式资源
                .setOutputCameraPath("/cstorPath")// 自定义相机输出目录，只针对Android Q以下，例如 Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) +  File.separator + "Camera" + File.separator;
                .cameraFileName("paizhao.png") // 重命名拍照文件名、注意这个只在使用相机时可以使用
                .renameCompressFile("yasuo.png")// 重命名压缩文件名、 注意这个不要重复，只适用于单张图压缩使用
                .renameCropFileName("jiancai.png")// 重命名裁剪文件名、 注意这个不要重复，只适用于单张图裁剪使用
                .isSingleDirectReturn(true)// 单选模式下是否直接返回，PictureConfig.SINGLE模式下有效
//                .setTitleBarBackgroundColor(getResources().getColor(R.color.white))//相册标题栏背景色
//                .isChangeStatusBarFontColor(false)// 是否关闭白色状态栏字体颜色
//                .setStatusBarColorPrimaryDark(getResources().getColor(R.color.white))// 状态栏背景色
//                .setUpArrowDrawable()// 设置标题栏右侧箭头图标
//                .setDownArrowDrawable()// 设置标题栏右侧箭头图标
                .isOpenStyleCheckNumMode(false)// 是否开启数字选择模式 类似QQ相册
                .selectionMode(PictureConfig.SINGLE)// 多选 or 单选 PictureConfig.MULTIPLE or PictureConfig.SINGLE
                .loadCacheResourcesCallback(GlideCacheEngine.createCacheEngine())
                .previewImage(true)// 是否可预览图片 true or false
                .previewVideo(false)// 是否可预览视频 true or false
                .enablePreviewAudio(false) // 是否可播放音频 true or false
                .isCamera(true)// 是否显示拍照按钮 true or false
                .imageFormat(PictureMimeType.PNG)// 拍照保存图片格式后缀,默认jpeg
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .sizeMultiplier(0.5f)// glide 加载图片大小 0~1之间 如设置 .glideOverride()无效
                .enableCrop(false)// 是否裁剪 true or false
//                .setCircleDimmedColor()// 设置圆形裁剪背景色值
//                .setCircleDimmedBorderColor()// 设置圆形裁剪边框色值
                .setCircleStrokeWidth(3)// 设置圆形裁剪边框粗细
                .compress(true)// 是否压缩 true or false
                .glideOverride(160, 160)// int glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .withAspectRatio(1, 1)// int 裁剪比例 如16:9 3:2 3:4 1:1 可自定义
                .hideBottomControls(true)// 是否显示uCrop工具栏，默认不显示 true or false
                .isGif(false)// 是否显示gif图片 true or false
                .compressSavePath(FileUtils.getPath())//压缩图片保存地址
                .freeStyleCropEnabled(true)// 裁剪框是否可拖拽 true or false
                .circleDimmedLayer(true)// 是否圆形裁剪 true or false
                .showCropFrame(false)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false   true or false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false    true or false
                .openClickSound(true)// 是否开启点击声音 true or false
//                .selectionMedia(selectList)// 是否传入已选图片 List<LocalMedia> list
                .previewEggs(true)// 预览图片时 是否增强左右滑动图片体验(图片滑动一半即可看到上一张是否选中) true or false
//                .cropCompressQuality(90)// 废弃 改用cutOutQuality()
                .cutOutQuality(90)// 裁剪输出质量 默认100
                .minimumCompressSize(100)// 小于100kb的图片不压缩
                .synOrAsy(true)//同步true或异步false 压缩 默认同步
                .cropImageWideHigh(1, 1)// 裁剪宽高比，设置如果大于图片本身宽高则无效
                .rotateEnabled(true) // 裁剪是否可旋转图片 true or false
                .scaleEnabled(true)// 裁剪是否可放大缩小图片 true or false
                .videoQuality(0)// 视频录制质量 0 or 1 int
                .videoMaxSecond(15)// 显示多少秒以内的视频or音频也可适用 int
                .videoMinSecond(10)// 显示多少秒以内的视频or音频也可适用 int
                .recordVideoSecond(0)//视频秒数录制 默认60s int
                .isDragFrame(true)// 是否可拖动裁剪框(固定)
                .forResult(code);//结果回调onActivityResult code

    }

    /**
     * 第三步：获取回调图片地址，转化为base64编码
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 回调成功
        switch (requestCode) {
            case 1: {
                dataParsing(data,1);
                break;
            }
            case 2: {
                dataParsing(data,2);
                break;
            }
        }
    }

    /**
     * 将图片转换成Base64编码的字符串
     */
    public void dataParsing(Intent data,int cede){
        // 图片选择结果回调
        selectList = PictureSelector.obtainMultipleResult(data);
        // 例如 LocalMedia 里面返回三种path
        // 1.media.getPath(); 为原图path
        // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
        // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
        // 如果裁剪并压缩了，已取压缩路径为准，因为是先裁剪后压缩的
        for (LocalMedia media : selectList) {
            if (media.isCompressed()) {//压缩
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(media.getCompressPath(), options);
                String type = options.outMimeType;
                if (TextUtils.isEmpty(type)) {
                    type = "未能识别的图片";
                } else {
                    //这里获取的图片的格式是以：”image/png”、”image/jpeg”、”image/gif”…….这样的方式返回的
                    //图片转base64编码并添加图片头。data:image/png;base64,
                    if (cede == 1){
                        Glide.with(mContext)
                                .load(media.getCompressPath())//传入加载的图片地址，网络地址或者本地图片都可以
                                .into(zuo);//imageview的id
                        base64zuo = "data:" + type + ";base64," + imageToBase64(media.getCompressPath());
                    }else if (cede==2){
                        Glide.with(mContext)
                                .load(media.getCompressPath())//传入加载的图片地址，网络地址或者本地图片都可以
                                .into(you);//imageview的id
                        base64you = "data:" + type + ";base64," + imageToBase64(media.getCompressPath());
                    }
                }
                Log.d("image type -> ", type);
            }
        }
    }

    /**
     * 将图片转换成Base64编码的字符串
     */
    public static String imageToBase64(String path){
        if(TextUtils.isEmpty(path)){
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try{
            is = new FileInputStream(path);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data,Base64.NO_CLOSE);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(null !=is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:{
                finish();
                break;
            }
            case R.id.zuo:{
                setPhoto(1);
                break;
            }
            case R.id.you:{
                setPhoto(2);
                break;
            }
            case R.id.iv_bidui:{
                HashMap<String,String> hashMap = new HashMap();
                if (null!=base64zuo && !base64zuo.equals("") && null!=base64you && !base64you.equals("")){
                    hashMap.put("img1",base64zuo);
                    hashMap.put("img2",base64you);
                    // 将map转换成json,需要引入Gson包
                    String mapToJson = new Gson().toJson(hashMap);
                    BaseRequest.postJsonData("http://ai.cstor.cn/api/face/compare", mapToJson, mHandler,1);
                }else {
                    Toast.makeText(mContext,"图片为空。",Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    /**
     * URLEncoded编码
     */

    public static String toURLEncoded(String paramString) {
        if (paramString == null || paramString.equals("")) {
            Log.d(TAG,"toURLEncoded error:"+paramString);
            return "";
        }

        try {
            String str = new String(paramString.getBytes(), "UTF-8");
            str = URLEncoder.encode(str, "UTF-8");
            return str;
        }
        catch (Exception localException) {
            Log.d(TAG,"toURLEncoded error:"+paramString, localException);
        }

        return "";
    }

}
